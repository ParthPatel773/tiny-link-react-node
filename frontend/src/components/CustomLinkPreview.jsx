import React, { useState, useEffect } from 'react';
import axios from 'axios';

const CustomLinkPreview = () => {
  const [url, setUrl] = useState('');
  const [metaInfo, setMetaInfo] = useState(null);
  const [showPreview, setShowPreview] = useState(false);

  useEffect(() => {
    setShowPreview(false);
  }, [url]);

  const fetchMetaInfo = async () => {
    try {
      const response = await axios.get(`http://localhost:5000/getMetadata?url=${url}`);
      const metaInfo = response.data;

      setMetaInfo(metaInfo);
      setShowPreview(true);
    } catch (error) {
      console.error('Error fetching meta info:', error);
    }
  };

  const handleLinkClick = () => {
    window.open(url, '_blank');
  };

  return (
    <div className="max-w-lg mx-auto p-4 border rounded-lg shadow-md w-[800px]">
      <h2 className="text-xl font-semibold mb-2">Custom Link Preview</h2>

      <div className="mb-4 w-full">
        <input
          type="text"
          placeholder="Enter URL"
          value={url}
          onChange={(e) => setUrl(e.target.value)}
          className="w-full p-2 border rounded-md"
        />
      </div>
      <button
        onClick={fetchMetaInfo}
        className="bg-blue-500 text-white px-4 py-2 rounded-md hover:bg-blue-600"
      >
        Fetch Preview
      </button>

      {showPreview && (
        <div className=" p-2 mt-5 rounded-md hover:bg-gray-100">
          <div className='border w-full'>
            <div className="p-4">
              {metaInfo && metaInfo.image ? (
                <div className='flex justify-center'>
                  <img
                    src={metaInfo.image}
                    alt="Preview"
                    className="w-64 h-64 object-cover rounded-md mb-2"
                  />
                </div>
              ) : (
                <div
                  className="w-full h-auto object-cover rounded-md mb-2 bg-gray-200 flex items-center justify-center"
                >
                  <p className="text-red-600">Image not available</p>
                </div>
              )}

              <h3 className="text-lg font-semibold">{metaInfo?.title}</h3>
              <p className="text-gray-600 text-sm">{metaInfo?.description}</p>
              <span
                className="text-blue-500 mt-2 block text-sm cursor-pointer"
                onClick={handleLinkClick}
              >
                {url}
              </span>
            </div>
          </div>

          <div className="border p-2 rounded-md hover:bg-gray-100 flex mt-4 w-full">
            <div className='flex items-center'>
              {metaInfo && metaInfo.image ? (
                <img
                  src={metaInfo.image}
                  alt="Preview"
                  className="w-32 h-32 object-cover rounded-md mr-4"
                />

              ) : (
                <div
                  className="w-32 h-32 object-cover rounded-md mb-2 bg-gray-200 flex items-center justify-center"
                >
                  <p className="text-red-600 text-center">Image not available</p>
                </div>
              )}
            </div>

            <div className="w-[66%]">
              <div className="p-4">
                <h3 className="text-lg font-semibold">{metaInfo.title}</h3>
                <p className="text-gray-600 text-sm">{metaInfo.description}</p>
              </div>
              <p
                className="text-blue-500 mt-2 block text-sm cursor-pointer p-4 truncate"
                onClick={handleLinkClick}
              >
                {url}
              </p>
            </div>
          </div>

        </div>
      )}
    </div>
  );
};

export default CustomLinkPreview;
