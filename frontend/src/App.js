import React from "react";
import CustomLinkPreview from "./components/CustomLinkPreview";

function App() {
  return (
    <div className="min-h-screen w-full flex items-center justify-center bg-gray-100">
      <CustomLinkPreview />
    </div>
  );
}

export default App;
