const express = require('express');
const cors = require('cors');
const axios = require('axios');
const cheerio = require('cheerio');

const app = express();
const port = 5000;
app.use(express.json({ extended: false }));
app.use(cors());

app.use("/api", app.get('/getMetadata', async (req, res) => {
  try {
    const { url } = req.query;

    const response = await axios.get(url);
    const html = response.data;

    const $ = cheerio.load(html);

    const title = $('meta[property="og:title"]').attr('content') || $('title').text();
    const description = $('meta[property="og:description"]').attr('content') || $('meta[name="description"]').attr('content');
    const image = $('meta[property="og:image"]').attr('content');

    res.json({ title, description, image, url });
  } catch (error) {
    res.status(500).json({ error: 'An error occurred' });
  }
}));

// app.listen(port, () => {
//   console.log(`Server is running on port ${port}`);
// });


app.listen(process.env.PORT || 3575, () => {
  console.log(`Server is running on port number ${process.env.PORT || 3575}`);
});
